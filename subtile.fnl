;;
;; Subtile V0.2.0
;;
;; Copyright (c) 2021 alexjgriffith
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy of
;; this software and associated documentation files (the "Software"), to deal in
;; the Software without restriction, including without limitation the rights to
;; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is furnished to do
;; so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.
;;

(var anim8 nil)

(local subtile {:_version "0.2.0"})

(fn subtile.newGrid [...]
    (anim8.newGrid ...))

(fn subtile.square4 [sample-grid x y]
    (sample-grid x y x (+ 1 y) (+ x 1) y (+ 1 x) (+ 1 y)))

(fn subtile.square16 [sample-grid x y]
  (local [a b] [(+ x 2) (+ y 0)])
  (local [c d] [(+ x 4) (+ y 0)])
  (local [e f] [(+ x 6) (+ y 0)])
  (sample-grid x y x (+ 1 y) (+ x 1) y (+ 1 x) (+ 1 y)
               a b a (+ 1 b) (+ a 1) b (+ 1 a) (+ 1 b)
               c d c (+ 1 d) (+ c 1) d (+ 1 c) (+ 1 d)
               e f e (+ 1 f) (+ e 1) f (+ 1 e) (+ 1 f)))

(fn subtile.square10 [sample-grid x y]
  (local [a b] [(+ x 2) (+ y 0)])
  (sample-grid x y x (+ 1 y) (+ 1 x) y (+ 1 x) (+ 1 y)
               a       b a       (+ 1 b) a       (+ 2 b)
               (+ 1 a) b (+ 1 a) (+ 1 b) (+ 1 a) (+ 2 b)
               (+ 2 a) b (+ 2 a) (+ 1 b) (+ 2 a) (+ 2 b)))

(fn subtile.rect [sample-grid x y w h]
    (let [squares {}]
      (for [i 0 (- w 1)]
           (for [j 0 (- h 1)]
                (tset squares (+ 1 (# squares)) (+ x i))
                (tset squares (+ 1 (# squares)) (+ y j))))
      (sample-grid (unpack squares) )))

;; batchSprite <- love.graphics.neSpriteBatch
(fn subtile.batch [batchSprite size x y tile-table [tr tl br bl] ?offset-x]
    (local offset-x (or ?offset-x 0))
      (: batchSprite :add (. tile-table tr)
         (+ offset-x (* (* x 2) size)) (* (* y 2) size))
      (: batchSprite :add (. tile-table tl)
         (+ offset-x (* (+ (* x 2) 1) size)) (* (* y 2) size))
      (: batchSprite :add (. tile-table br)
         (+ offset-x (* (* x 2) size)) (* (+ (* y 2) 1) size))
      (: batchSprite :add (. tile-table bl)
         (+ offset-x (* (+ (* x 2) 1) size)) (* (+ (* y 2) 1) size)))

(fn subtile.batch-rect [batchSprite size x y tile-table w h map-width ?offset-x]
    (local offset-x (or ?offset-x 0))
    (var k 0)
    (var xp x)
    (for [i 0  (- w 1) ]
         (for [j 0 (- h 1)]
              (set k (+ 1 k))
              (if (>= (+ (* x 2) i) (* map-width 2))
                  (set xp (- x map-width))
                  (< (+ (* x 2) i) 0)
                  (set xp (- map-width x))
                  (set xp x))
              (: batchSprite :add (. tile-table k)
                 (+ offset-x (* (+ (* xp 2) i)  size)) (* (+ (* y 2) j)  size)))))

(fn subtile.fixed [...]
    [1 3 2 4])

(fn subtile.fence [type neigh]
    (let [sel
          (fn [x y] (let [xp (if (= x type) 1 0)
                          yp (if (= y type) 1 0)]
                      (+ 1 xp (* 2 yp))))
          a
          (. [1 1 5 5] (sel neigh.up neigh.left))
          b
          (. [3 3 7 7] (sel neigh.up neigh.right))
          c
          (. [2 10 14 6] (sel neigh.down neigh.left))
          d
          (. [4 12 16 8] (sel neigh.down neigh.right))]
      [a b c d]))


(fn subtile.blob [type neigh]
    (let [sel
          (fn [x y z]
            (let [xp (if (or (= x type) (= x :edge)) 1 0)
                  yp (if (or (= y type) (= y :edge)) 1 0)
                  zp (if (or (= z type) (= z :edge)) 1 0)]
              (+ 1 xp (* 2 yp) (* 4 zp))))
          a
          (. [5 6 5 6 8 4 8 9] (sel neigh.up neigh.up-left neigh.left))
          b
          (. [11 12 11 12 8 2 8 9] (sel neigh.up neigh.up-right neigh.right))
          c
          (. [7 6 7 6 10  3 10 9] (sel neigh.down neigh.down-left neigh.left))
          d
          (. [13 12 13 12 10 1 10 9] (sel neigh.down neigh.down-right neigh.right))]
      [a b c d]))


;; debugging only
(fn subtile.neighbour-options []
    (let [tab {}]
      (for [i 0 255]
           (tset tab i {})
           (each [key value
                      (ipairs [:right :left
                                      :up :down
                                      :up-right :up-left
                                      :down-right :down-left])]
                 (tset  tab i value
                        (% (math.floor (/ i (math.pow 2 (- key 1)) )) 2))))
       tab))

(fn [{:anim8 in-anim8 }]
  (set anim8 (require in-anim8))
  subtile)
