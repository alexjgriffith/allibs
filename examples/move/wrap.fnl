(global gamestate (require :gamestate))
(local repl (require :stdio))

(fn love.load []
  (love.graphics.setDefaultFilter "nearest" "nearest")
  (gamestate.registerEvents)
  (gamestate.switch (require :move) :wrap)
  (repl.start))
