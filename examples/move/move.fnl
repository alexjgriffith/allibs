(local move {})

(local libs {:anim8 :anim8 :json :json :lume :lume})

(local loader ((require :loader) libs))

(local movement (. ((require :movement) libs) :topdown))

(local keybindings {:left  [:a :left] :right [:d :right]
                    :down  [:s :down] :up    [:w :up]})

(local blue-colour [0.371 0.801 0.891 1])

(local sprite (loader.load-four "assets/data/Alicia" "assets/sprites"))

(local player {:pos {:x 100 :y 100}
               :speed {:x 0 :y 0 :rate 20 :decay 20
                       :max 2 :max-s2 (/ 2 (math.sqrt 2)) :floor 0.2}
               :dir :forward
               :anim :forward-base})

(fn player-update-controler [dt]
  (let [(_x _y dir moving) (movement dt keybindings player)
        state (match moving true :walk false :base)
        anim (.. dir "-" state)]
    (when (~= anim player.anim)
      (sprite:reset anim)
      (tset player :anim anim))
    anim))

(fn move.update [self dt]
  (let [anim (player-update-controler dt)]
    (sprite:update anim dt)))

(fn move.draw [self]
  (love.graphics.setColor blue-colour)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (sprite:draw player.anim player.pos 2))

move
