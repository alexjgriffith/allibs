(global gamestate (require :gamestate))
(local repl (require :stdio))

(fn love.load []
  (gamestate.registerEvents)
  (gamestate.switch (require :menu) :wrap)
  (repl.start))
