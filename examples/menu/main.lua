-- bootstrap the compiler
package.path = "../lib/?.lua;" .. package.path

local fennel_module = "fennel"
fennel = require (fennel_module)
table.insert(package.loaders, fennel.make_searcher({correlate = true,
                                                    moduleName = fennel_module,
                                                    useMetadata = true,}))

package.loaded.fennel = fennel

fennel.path = love.filesystem.getSource() .. "/?.fnl;" ..
   "../../?.fnl;" ..
   "../lib/?.fnl;" ..
   fennel.path

pp = function(x) print(require("fennelview")(x)) end

lume = require("lume")

require("wrap")
