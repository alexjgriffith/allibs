;;; menu.fnl
(local menu {})

(local gamestate (require :gamestate))

(local buttons ((require :buttons)))
(local colour ((require :colour)))

(fn avara [size]
   (love.graphics.newFont "assets/fonts/avara.otf" size))

(local params
       {:colours {:background  (colour.hex-to-rgba "#222034")
                  :hover-button-background  (colour.hex-to-rgba "#ffffff")
                  :button-background  (colour.hex-to-rgba "#222034")
                  :text  (colour.hex-to-rgba "#ffffff")
                }
        :screen-width 1280
        :screen-height 720})

(local element-font
       {:title  (avara 100)
        :subtitle  (avara 40)
        :text  (avara 20)
        :button  (avara 40)})

(local element-click
       {"Quit Game"
        (fn []
          (love.event.quit))})

(local element-hover {:button (fn [element x y] :nothing)})

(local elements [{:type :title :y 150 :oy 0 :ox 0 :w 400 :h 60 :text "Some Title"}
                 {:type :button :y 350 :oy -10 :ox 0 :w 400 :h 70 :text "Quit Game"}
                  ])

(local ui (buttons elements params element-click element-hover element-font))

(fn menu.draw [self]
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw))

(fn menu.update [self dt]
  (ui:update dt))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

(fn menu.keypressed [self key code]
  (match key
    :escape (love.event.quit)))

menu
